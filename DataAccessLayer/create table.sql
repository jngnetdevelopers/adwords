-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 26, 2018 at 01:29 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ppc_reporting_dbo`
--

-- --------------------------------------------------------

--
-- Table structure for table `adwords_daily_report`
--

CREATE TABLE `adwords_daily_report` (
  `Campaign_id` bigint(20) NOT NULL,
  `Date` text,
  `Currency` text,
  `Clicks` bigint(20) NOT NULL,
  `Impressions` bigint(20) NOT NULL,
  `CTR` double NOT NULL,
  `Avg_CPC` double NOT NULL,
  `Avg_CPM` double NOT NULL,
  `Conversions` double NOT NULL,
  `Conv_rate` double NOT NULL,
  `All_Conv_value` double NOT NULL,
  `Cost_Per_Conv` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


