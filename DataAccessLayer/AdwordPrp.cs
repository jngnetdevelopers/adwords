﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
   public  class AdwordPrp
    {
        public long Campaign_id { get; set; }
        public string Date { get; set; }
        public string Currency { get; set; }
        public long Clicks { get; set; }
        public long Impressions { get; set; }
        public double CTR { get; set; }
        public decimal Avg_CPC { get; set; }
        public decimal Avg_CPM { get; set; }
        public double Conversions { get; set; }
        public double Conv_rate { get; set; }
        public double All_Conv_value { get; set; }
        public decimal Cost_Per_Conv { get; set; }
    }
}
