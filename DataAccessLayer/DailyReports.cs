﻿using System;
using MySql.Data.MySqlClient;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.Common.Lib;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace DataAccessLayer
{
    public class DailyReports
    {
        string cs = ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        string filePath = "";
        AdWordsAppConfig config;
        MySqlConnection conn = null;
        static AdWordsUser user;
        AdwordPrp adwordsApiServiceObj = new AdwordPrp();
        public DailyReports()
        {
            user = new AdWordsUser();
            (user.Config as AdWordsAppConfig).ClientCustomerId = "340-609-2602";
            (user.Config as AdWordsAppConfig).OAuth2ClientId = "441624344725-8e8b992f67kcnnuafjfpsgl91dbg0sot.apps.googleusercontent.com";
            (user.Config as AdWordsAppConfig).OAuth2ClientSecret = "-6iPr1ELltbYihv3ek2EF1gg";

            config = user.Config as AdWordsAppConfig;
            if (user.Config.OAuth2Mode == OAuth2Flow.APPLICATION && string.IsNullOrEmpty(config.OAuth2RefreshToken))
            {
                DoAuth2Authorization(user);
            }

            //File Path
            conn = new MySqlConnection(cs);

        }
        private static void DoAuth2Authorization(AdWordsUser user)
        {
            // Since we are using a console application, set the callback url to null.
            user.Config.OAuth2RedirectUri = null;
            AdsOAuthProviderForApplications oAuth2Provider =
                (user.OAuthProvider as AdsOAuthProviderForApplications);
            // Get the authorization url.
            var authorizationUrl = oAuth2Provider.GetAuthorizationUrl();
            Console.WriteLine("Open a fresh web browser and navigate to \n\n{0}\n\n. You will be " +
                "prompted to login and then authorize this application to make calls to the " +
                "AdWords API. Once approved, you will be presented with an authorization code.",
                authorizationUrl);

            // Accept the OAuth2 authorization code from the user.
            Console.Write("Enter the authorization code :");
            string authorizationCode = Console.ReadLine();

            // Fetch the access and refresh tokens.
            oAuth2Provider.FetchAccessAndRefreshTokens(authorizationCode);
        }

        //Insert

        public string InsertDailyReport(string reportData)
        {
            
            string msg = "";
            if (!string.IsNullOrEmpty(reportData))
            {
                try
                {
                   

                    #region new

                    string[] strSub = reportData.Split('\n');

                    foreach (var item in strSub)
                    {
                        if (item != "")
                        {
                            string[] strArr = item.Split(',');
                            long Campaign_id = Convert.ToInt64(strArr[0]);
                            string Date = Convert.ToString(strArr[1]);
                            string Currency = Convert.ToString(strArr[2]);
                            long Clicks = Convert.ToInt64(strArr[3]);
                            long Impressions = Convert.ToInt64(strArr[4]);
                            var strCTR = strArr[5].Split('%');
                            double CTR = Convert.ToDouble(strCTR[0]);
                            decimal Avg_CPC = Convert.ToDecimal(strArr[6]);
                            decimal Avg_CPM = Convert.ToDecimal(strArr[7]);
                            double Conversions = Convert.ToDouble(strArr[8]);
                            var strConv_rate = strArr[9].Split('%');
                            double Conv_rate = Convert.ToDouble(strConv_rate[0]);
                            double All_Conv_value = Convert.ToDouble(strArr[10]);
                            decimal Cost_Per_Conv = Convert.ToDecimal(strArr[11]);

                            MySqlCommand cmd = conn.CreateCommand();
                            cmd.CommandText = "insert into adwords_daily_report(Campaign_id,Date,Currency,Clicks,Impressions,CTR,Avg_CPC,Avg_CPM,Conversions,Conv_rate,All_Conv_value,Cost_Per_Conv) values (" + Campaign_id + ",'" + Date + "','" + Currency + "'," + Clicks + ","
                            + Impressions + "," + CTR + "," + Avg_CPC + "," + Avg_CPM + "," + Conversions + "," + Conv_rate + "," + All_Conv_value + "," + Cost_Per_Conv + ")";
                            cmd.Connection = conn;
                            if (conn.State == System.Data.ConnectionState.Closed)
                            {
                                conn.Open();
                            }
                            cmd.ExecuteNonQuery();
                            cmd.Dispose();

                            msg = "Data is uploaded successfully";
                        }
                    }


                    #endregion
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally
                {
                    conn.Close();
                }
            }

            return msg;
        }
     
        //Get daily report by campaign id
        public List<AdwordPrp> getDailyReports(int campaignID)
        {
            var liObj = new List<AdwordPrp>();
            MySqlDataReader dr = null;
            try
            {
                var strCn = "select * from adwords_daily_report where Campaign_Id = " + campaignID;
                var cmd = new MySqlCommand(strCn, conn);

                if (conn.State == System.Data.ConnectionState.Closed)
                {
                    conn.Open();
                }

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    adwordsApiServiceObj = new AdwordPrp();
                    adwordsApiServiceObj.Campaign_id = Convert.ToInt64(dr[0]);
                    adwordsApiServiceObj.Date = Convert.ToString(dr[1]);
                    adwordsApiServiceObj.Currency = Convert.ToString(dr[2]);
                    adwordsApiServiceObj.Clicks = Convert.ToInt64(dr[3]);
                    adwordsApiServiceObj.Impressions = Convert.ToInt64(dr[4]);
                    adwordsApiServiceObj.CTR = Convert.ToDouble(dr[5]);
                    adwordsApiServiceObj.Avg_CPC = Convert.ToDecimal(dr[6]);
                    adwordsApiServiceObj.Avg_CPM = Convert.ToDecimal(dr[7]);
                    adwordsApiServiceObj.Conversions = Convert.ToDouble(dr[8]);
                    adwordsApiServiceObj.Conv_rate = Convert.ToDouble(dr[9]);
                    adwordsApiServiceObj.All_Conv_value = Convert.ToDouble(dr[10]);
                    adwordsApiServiceObj.Cost_Per_Conv = Convert.ToDecimal(dr[11]);
                    liObj.Add(adwordsApiServiceObj);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                dr.Close();
                conn.Close();
            }
            return liObj;
        }

        //Get daily report by campaign id and date
        public List<AdwordPrp> getDailyReport(int campaignID, string date)
        {
            var liObj = new List<AdwordPrp>();
            MySqlDataReader dr = null;
            try
            {
                string strCn = "select * from adwords_daily_report where Campaign_Id = " + campaignID + " and Date ='" + date + "'";
                var cmd = new MySqlCommand(strCn, conn);
                if (conn.State == System.Data.ConnectionState.Closed)
                {
                    conn.Open();
                }
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    adwordsApiServiceObj.Campaign_id = Convert.ToInt64(dr[0]);
                    adwordsApiServiceObj.Date = Convert.ToString(dr[1]);
                    adwordsApiServiceObj.Currency = Convert.ToString(dr[2]);
                    adwordsApiServiceObj.Clicks = Convert.ToInt64(dr[3]);
                    adwordsApiServiceObj.Impressions = Convert.ToInt64(dr[4]);
                    adwordsApiServiceObj.CTR = Convert.ToDouble(dr[5]);
                    adwordsApiServiceObj.Avg_CPC = Convert.ToDecimal(dr[6]);
                    adwordsApiServiceObj.Avg_CPM = Convert.ToDecimal(dr[7]);
                    adwordsApiServiceObj.Conversions = Convert.ToDouble(dr[8]);
                    adwordsApiServiceObj.Conv_rate = Convert.ToDouble(dr[9]);
                    adwordsApiServiceObj.All_Conv_value = Convert.ToDouble(dr[10]);
                    adwordsApiServiceObj.Cost_Per_Conv = Convert.ToDecimal(dr[11]);
                    liObj.Add(adwordsApiServiceObj);
                }
                //conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                conn.Close();
                dr.Close();

            }
            return liObj;
        }

    }
}
