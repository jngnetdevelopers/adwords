﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace BusinessLayer
{
   public class AdwordsApiService
    {

        public long Campaign_id  { get; set; }
        public DateTime Date { get; set; }
        public decimal Currency { get; set; }
        public long Clicks { get; set; }

        //An impression is counted each time your ad is shown on a search result page or other site on the Google Network.
        public long Impressions { get; set; }

        //CTR is the number of clicks that your ad receives divided by the number of times your ad is shown: clicks ÷ impressions = CTR.
        public double CTR { get; set; }

        //The average amount that you've been charged for a click on your ad. Average cost-per-click (avg. CPC) is calculated by dividing the total cost of your clicks by the total number of clicks.
        public decimal Avg_CPC { get; set; }
        //Cost-per-thousand impressions (CPM)
       // CPM bidding means that you pay based on the number of impressions(times your ads are shown) that you receive on the Google Display Network.
        public decimal Avg_CPM { get; set; }
             

       // Conversion: Definition.An action that's counted when someone interacts with your ad (for example, clicks a text ad or views a video ad) and then takes an action that you've defined as valuable to your business, such as an online purchase or a call to your business from a mobile phone.
        public double Conversions { get; set; }
        //Conversion rates are calculated by simply taking the number of conversions and dividing that by the number of total ad clicks that can be tracked to a conversion during the same time period. For example, if you had 50 conversions from 1,000 clicks, your conversion rate would be 5%, since 50 ÷ 1,000 = 5%.
        public double Conv_rate { get; set; }

        //Total conversion value is the sum of all conversion values for all conversions.
        //Its the total revenue earned by the keyword in a day. Suppose if a keywords lead to a sales of 3 cars worth $10 and 1 bus worth $5 then the total conversion value of the keywords on that particular day will be $15.
        public double All_Conv_value { get; set; }
        public decimal Cost_Per_Conv { get; set; }
               
    }
}
