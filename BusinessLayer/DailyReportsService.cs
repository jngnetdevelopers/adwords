﻿
using DataAccessLayer;
using System.Collections.Generic;

namespace BusinessLayer
{
   
    public class DailyReportsService
    {
        DailyReports objadWordApiServiceDAL = new DailyReports();
        AdwordPrp adwordsApiServiceObj = new AdwordPrp();
       
        public string InsertDailyReport(string reportData)
        {
          string msg=  objadWordApiServiceDAL.InsertDailyReport(reportData);
            return msg;
        }

        public List<AdwordPrp> getDailyReports(int campaignID)
        {
            List<AdwordPrp> k = objadWordApiServiceDAL.getDailyReports(campaignID);
                return k;
        }
        public List<AdwordPrp> getDailyReport(int campaignID, string date)
        {
            List<AdwordPrp> k=objadWordApiServiceDAL.getDailyReport(campaignID, date);
            return k;

        }

    }
}
