﻿using System;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.Util.Reports;
using Google.Api.Ads.AdWords.v201705;
using Google.Api.Ads.Common.Lib;
using Google.Api.Ads.Common.Util.Reports;
using BusinessLayer.Examples.CSharp;
using System.IO;
using DataAccessLayer;
using System.Text;
using System.Collections.Generic;

namespace BusinessLayer
{
    public class AdwordsApiService
    {
        string filePath = "";
        AdWordsAppConfig config;
        static AdWordsUser user;
        DailyReports objadWordApiServiceDAL = new DailyReports();
        public AdwordsApiService()
        {
            user = new AdWordsUser();
            (user.Config as AdWordsAppConfig).ClientCustomerId = "340-609-2602";
            (user.Config as AdWordsAppConfig).OAuth2ClientId = "441624344725-8e8b992f67kcnnuafjfpsgl91dbg0sot.apps.googleusercontent.com";
            (user.Config as AdWordsAppConfig).OAuth2ClientSecret = "-6iPr1ELltbYihv3ek2EF1gg";

            config = user.Config as AdWordsAppConfig;
            if (user.Config.OAuth2Mode == OAuth2Flow.APPLICATION && string.IsNullOrEmpty(config.OAuth2RefreshToken))
            {
                DoAuth2Authorization(user);
            }
        }

        //Get daily report by campaign id

        private static void DoAuth2Authorization(AdWordsUser user)
        {
            // Since we are using a console application, set the callback url to null.
            user.Config.OAuth2RedirectUri = null;
            AdsOAuthProviderForApplications oAuth2Provider =
                (user.OAuthProvider as AdsOAuthProviderForApplications);
            // Get the authorization url.
            string authorizationUrl = oAuth2Provider.GetAuthorizationUrl();
            Console.WriteLine("Open a fresh web browser and navigate to \n\n{0}\n\n. You will be " +
                "prompted to login and then authorize this application to make calls to the " +
                "AdWords API. Once approved, you will be presented with an authorization code.",
                authorizationUrl);

            // Accept the OAuth2 authorization code from the user.
            Console.Write("Enter the authorization code :");
            string authorizationCode = Console.ReadLine();

            // Fetch the access and refresh tokens.
            oAuth2Provider.FetchAccessAndRefreshTokens(authorizationCode);
        }

        public string GetDailyReports(int campaignID)
        {
            StringBuilder MyStrBuilder = new StringBuilder();
            var definition = new ReportDefinition()
            {
                reportName = "CAMPAIGN_PERFORMANCE_REPORT",
                reportType = ReportDefinitionReportType.CAMPAIGN_PERFORMANCE_REPORT,
                downloadFormat = DownloadFormat.CSV,
                dateRangeType = ReportDefinitionDateRangeType.LAST_7_DAYS,
                selector = new Selector()
                {
                    fields = new string[] {"CampaignId", "Date", "AccountCurrencyCode", "Clicks", "Impressions",
                "Ctr", "AverageCpc", "AverageCpm", "Conversions","ConversionRate","AllConversionValue","CostPerConversion"},
                    predicates = new Predicate[] {
              Predicate.In("CampaignId", new string[] {Convert.ToString(campaignID) })
            }
                }
            };

            config.IncludeZeroImpressions = true;
            try
            {
                //To download AdWords API report using the ReportUtilities class
                var utilities = new ReportUtilities(user, "v201705", definition);
                using (ReportResponse response = utilities.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.Stream))
                    {
                        string tables = null;
                        while ((tables = reader.ReadLine()) != null)
                        {
                            if (!tables.Contains("Campaign ID") && !tables.Contains("CAMPAIGN_PERFORMANCE_REPORT") && !tables.Contains("Total"))
                            {
                                MyStrBuilder.Append(tables + "\n");
                            }
                        }
                       
                    }
                }
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Failed to download report.", e);
            }
            return MyStrBuilder.ToString();
        }
    }
}
