﻿using System;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.Common.Lib;
using WU.Outsourcing.Base.Examples.CSharp;
using BusinessLayer;
using System.IO;
using System.Collections.Generic;
using DataAccessLayer;
using System.Text.RegularExpressions;

namespace WU.Outsourcing.Base
{
    class Program : System.Web.UI.Page
    {
        static AdWordsUser user;
        private static void DoAuth2Authorization(AdWordsUser user)
        {
            
            user.Config.OAuth2RedirectUri = null;
            AdsOAuthProviderForApplications oAuth2Provider =
                (user.OAuthProvider as AdsOAuthProviderForApplications);
            // Get the authorization url.
            string authorizationUrl = oAuth2Provider.GetAuthorizationUrl();
            Console.WriteLine("Open a fresh web browser and navigate to \n\n{0}\n\n. You will be " +
                "prompted to login and then authorize this application to make calls to the " +
                "AdWords API. Once approved, you will be presented with an authorization code.",
                authorizationUrl);

            // Accept the OAuth2 authorization code from the user.
            Console.Write("Enter the authorization code :");
            string authorizationCode = Console.ReadLine();

            // Fetch the access and refresh tokens.
            oAuth2Provider.FetchAccessAndRefreshTokens(authorizationCode);
        }

       
        static void Main(string[] args)
        {
            var objAdword = new AdwordsApiService();
            var objDailyReport = new DailyReportsService();
            user = new AdWordsUser();
            (user.Config as AdWordsAppConfig).ClientCustomerId = "340-609-2602";
            (user.Config as AdWordsAppConfig).OAuth2ClientId = "441624344725-8e8b992f67kcnnuafjfpsgl91dbg0sot.apps.googleusercontent.com";
            (user.Config as AdWordsAppConfig).OAuth2ClientSecret = "-6iPr1ELltbYihv3ek2EF1gg";

            try
            {
                Console.WriteLine("Please Enter Campaign id \n");
                var cid = Convert.ToInt32(Console.ReadLine());
                string filePath = ExampleUtilities.GetHomeDir() + Path.DirectorySeparatorChar + "CAMPAIGN_PERFORMANCE_REPORT";
                
                Console.WriteLine("1 for get the data from api");
                Console.WriteLine("2 for get the data from api and write into csv and insert into Database");
                Console.WriteLine("3 for  get data from table by campaign id");
                Console.WriteLine("4 for  get data from table by campaign id and date");
                Console.WriteLine("Other than 1,2,3,4 choice for exit");
                while (true)
                {
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("Please Enter Choice : ");

                    var choice = Convert.ToInt32(Console.ReadLine());
                    switch (choice)
                    {
                        case 1:
                            var s = objAdword.GetDailyReports(cid);
                              Console.WriteLine("{0}  ", s);
                            break;
                        case 2:
                            var s1 = objAdword.GetDailyReports(cid);
                           string msg= objDailyReport.InsertDailyReport(s1);
                            if (msg != "")
                                Console.WriteLine(msg);
                            
                            break;

                        case 3:
                            List<AdwordPrp> liobj = objDailyReport.getDailyReports(cid);

                            foreach (var item in liobj)
                            {
                                Console.WriteLine(item.Campaign_id.ToString() + "," + item.Date.ToString() + "," + item.Currency.ToString() + "," + item.Clicks.ToString() + "," + item.Impressions.ToString() + "," + item.CTR.ToString() + "," + item.Avg_CPC.ToString()
                                    + "," + item.Avg_CPM.ToString() + "," + item.Conversions.ToString() + "," + item.Conv_rate.ToString() + "," + item.All_Conv_value.ToString() + "," + item.Cost_Per_Conv.ToString());
                            }

                            break;
                        case 4:
                            T:
                            Console.WriteLine("Enter Date in the yyyy-mm-dd format");
                            string date = Console.ReadLine();
                            
                            Regex r = new Regex(@"\d{4}-\d{2}-\d{2}");

                          bool b=  r.IsMatch(date);
                            if (b)
                            {
                                List<AdwordPrp> liobjByIdDate = objDailyReport.getDailyReport(cid, date);

                                foreach (var item in liobjByIdDate)
                                {
                                    Console.WriteLine(item.Campaign_id.ToString() + "," + item.Date.ToString() + "," + item.Currency.ToString() + "," + item.Clicks.ToString() + "," + item.Impressions.ToString() + "," + item.CTR.ToString() + "," + item.Avg_CPC.ToString()
                                        + "," + item.Avg_CPM.ToString() + "," + item.Conversions.ToString() + "," + item.Conv_rate.ToString() + "," + item.All_Conv_value.ToString() + "," + item.Cost_Per_Conv.ToString());
                                }
                            }
                            else
                            {
                               // Console.WriteLine("Enter date in correct format(yyyy-mm-dd)");
                                goto T;  
                            }
                            break;
                        default:
                            Environment.Exit(0);
                            return;

                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadKey();
        }
    }
}
